import Vue from 'vue';

new Vue({
  el: '#app',
  data: {
    data: [],
    speakers: [],
    key: '1hFVnzwlccXF9lQd1GsJL3_bdK2vVTOPQe8AAK0tlpPQ',
    schedule_sheet: 1,
    speaker_sheet: 2,
    currentDate: '28 Jan 2021'
  },
  computed: {
    schedule() {
      return this.data.filter(item => item.date == this.currentDate);
    },
    track1() {
      return this.schedule.filter(item => item.track == 1);
    },
    track2() {
      return this.schedule.filter(item => item.track == 2);
    },
    track3() {
      return this.schedule.filter(item => item.track == 3);
    },
    track4() {
      return this.schedule.filter(item => item.track == 4);
    }
  },
  methods: {
    loadData: function(sheet, item) {
      var vm = this;
      fetch(`https://gsapi.futurelabkerala.in/api?id=${this.key}&sheet=${sheet}&columns=false&integers=false`)
        .then(response => response.json())
        .then(function(json) {
          vm[item] = json.rows;
        });
    },
    changeSched(date) {
      this.currentDate = date
    }

  },
  mounted() {
    this.loadData(1, 'data');
    this.loadData(2, 'speakers');
  }

});