import 'animate.css';

//select all slides
const getSlides = document.querySelectorAll(".slide");

//set slide count
let currentSlide = 0;

//increment slide index
const nextSlide = () => {
  changeSlide(currentSlide + 1);
};

//decrement slide index
const prevSlide = () => {
  changeSlide(currentSlide - 1);
};

//select arrow controls
const next = document.querySelector(".right-arrow");
const prev = document.querySelector(".left-arrow");

//click event that passes to changeSlide function
next.addEventListener("click", nextSlide, false);
prev.addEventListener("click", prevSlide, false);

const changeSlide = clicked => {
  //selects class for current slide
  getSlides[currentSlide].className = "slide";

  //checks remainder of link to select current slide
  currentSlide = (clicked + getSlides.length) % getSlides.length;

  //adds css class to show current slide
  getSlides[currentSlide].className = "slide show";
};

setInterval(nextSlide, 4000);

var modals = document.querySelectorAll('a[data-modal]');
for (var modal of modals) {
  modal.addEventListener('click', function(e) {
    e.preventDefault();
    var attributes = e.target.attributes;
    var id = attributes['data-modal'].value
    var modal_cont = document.getElementById(id)
    modal_cont.style.transform = "scaleY(1)"

    modal_cont.querySelector('.close-btn').addEventListener('click', function() {
      modal_cont.style.transform = "scaleY(0)"
    })

  })
}

// var closeBtns = document.querySelectorAll('a[data-close]');
// for (var btn of closeBtns) {
//   btn.addEventListener('click', function(e) {
//     e.preventDefault();
//     var attributes = e.target.attributes;
//     console.log(attributes)
//     var id = attributes['data-close'].value
//     document.getElementById(id).style.transform = "scale(0)"
//   })
// }